package com.example.carsmemory.view

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.ImageView
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.example.carsmemory.R
import com.example.carsmemory.databinding.ActivityHardGameBinding
import com.example.carsmemory.model.Card
import com.example.carsmemory.viewmodel.GameViewModel


class HardGame : AppCompatActivity() {

    private lateinit var cardsButtons: List<ImageView>
    private lateinit var cards: List<Card>
    private var indexOfSingleSelectedCard: Int? = null
    var countCardMatcheds = 0
    var countMatcheds = 0
    var countNoMatcheds = 0
    var score = 0
    val levelName = "HardGame"
    val gameViewModel: GameViewModel by viewModels()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = ActivityHardGameBinding.inflate(layoutInflater)
        setContentView(binding.root)


        val timerText = binding.timer
        gameViewModel.currentTime.observe(this, Observer {
            timerText.text = it.toString()})

        val movementsText = binding.movements
        gameViewModel.countMovements.observe(this, Observer {
            movementsText.text = "Movements: $it" })



        val images = mutableListOf(R.drawable.car1, R.drawable.car2, R.drawable.car3, R.drawable.car4)
        images.addAll(images)
        images.shuffle()

        cardsButtons = listOf(binding.card1,binding.card2,binding.card3,binding.card4,binding.card5,binding.card6, binding.card7,binding.card8)

        cards = cardsButtons.indices.map { index ->
            Card(images[index])
        }

        cardsButtons.forEachIndexed{ index, cardButton ->
            cardButton.setOnClickListener {
                updateModels(index)
                updateViews()
                gameViewModel.updateCountMovements()
            }
        }

        val pauseButton = binding.pauseButton
        val pauseMenuImage = binding.pauseMenuImage
        val resumeMenuButton = binding.resumeMenuButton
        val mainMenuButton = binding.mainMenuMenuButton
        val exitMenuButton = binding.exitMenuButton
        val menuBackground = binding.menuBackgroud
        pauseButton.setOnClickListener {
            gameViewModel.timer.cancel()
            menuBackground.visibility = View.VISIBLE
            pauseMenuImage.visibility = View.VISIBLE
            resumeMenuButton.visibility = View.VISIBLE
            mainMenuButton.visibility = View.VISIBLE
            exitMenuButton.visibility = View.VISIBLE
            resumeMenuButton.setOnClickListener {
                pauseMenuImage.visibility = View.INVISIBLE
                resumeMenuButton.visibility = View.INVISIBLE
                mainMenuButton.visibility = View.INVISIBLE
                exitMenuButton.visibility = View.INVISIBLE
                menuBackground.visibility = View.INVISIBLE
                gameViewModel.setPauseTime()
            }
            mainMenuButton.setOnClickListener {
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
            exitMenuButton.setOnClickListener {
                finishAffinity()
            }
        }

        gameViewModel.gameFinish.observe(this, Observer {
            if (it){
                calculeScore()
                goToResultScreen()
            }
        })
    }

    private fun updateViews() {
        cards.forEachIndexed { index, card ->
            val cardButton = cardsButtons[index]
            cardButton.setImageResource(if (card.isFaceUp) card.id else R.drawable.card_image)
        }
    }

    private fun updateModels(position:Int) {
        val card = cards[position]
        //Tres casos
        //0 cartas giradas previamente -> restaurar las cartas + girar la carta
        //1 cartas giradas previamente -> girar la carta + mirar si las imagenes coinciden
        //2 cartas giradas previamente -> restaurar las cartas + girar la carta
        if (indexOfSingleSelectedCard == null){
            // 0 o 2 cartas seleccionadas previamente
            restoreCards()
            indexOfSingleSelectedCard = position
        }else{
            // solo 1 carta seleccionada previamente
            checkForMatch(indexOfSingleSelectedCard!!, position)
            indexOfSingleSelectedCard = null
        }
        card.isFaceUp = !card.isFaceUp
    }

    private fun restoreCards() {
        for (card in cards){
            if (!card.isMatched){
                card.isFaceUp = false
            }
        }
    }

    private fun checkForMatch(position1: Int, position2: Int) {
        if (cards[position1].id == cards[position2].id){
            cards[position1].isMatched = true
            cards[position2].isMatched = true
            cardsButtons[position1].setOnClickListener {  }
            cardsButtons[position2].setOnClickListener {  }
            countCardMatcheds += 2
            countMatcheds ++
            if (countCardMatcheds==cardsButtons.size){
                gameViewModel.timer.cancel()
                calculeScore()
                goToResultScreen()
            }
        }else countNoMatcheds++
    }

    private fun calculeScore() {
        gameViewModel.currentTime.observe(this, Observer {
            score = (countMatcheds*20)+(countNoMatcheds*(-5)+it.toInt())})
    }


    private fun goToResultScreen() {
        val intent = Intent(this, resultScreen::class.java)
        intent.putExtra("score", score.toString())
        intent.putExtra("levelName", levelName)
        Handler(Looper.getMainLooper()).postDelayed({
            startActivity(intent)
        }, 2000)
    }
}