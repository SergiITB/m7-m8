package com.example.carsmemory.viewmodel

import android.os.CountDownTimer
import android.widget.Chronometer
import android.widget.ImageView
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.carsmemory.R
import com.example.carsmemory.model.Card
import com.example.carsmemory.model.CardProvider
import java.util.Collections.shuffle

class GameViewModel: ViewModel() {

    //DECLARACION DE VARIABLES
    val cards = mutableListOf(MutableLiveData<Card>())
    var COUNTDOWN_TIMER = 30000L
    val ONE_SECOND = 1000L
    val countMovements = MutableLiveData<Int>()
    var timer:CountDownTimer
    var currentTime = MutableLiveData<Long>()
    val gameFinish = MutableLiveData<Boolean>()
    private var images = arrayOf(
        R.drawable.car1,
        R.drawable.car2,
        R.drawable.car3,
        R.drawable.car4,
        R.drawable.car1,
        R.drawable.car2,
        R.drawable.car3,
        R.drawable.car4)
    val cards = mutableListOf<Card>()
    private var movements = 0
    private var indexOfSingleSelectedCard: Int? = null
    private var countCardMatcheds = 0
    private var countMatcheds = 0
    private var countNoMatcheds = 0
    var score = 0

    //INIT
    init {
        images.shuffle()
        for (i in 0..7){
            cards.add(Card(i,images[i]))
        }

        countMovements.value=0
        timer = object:CountDownTimer(COUNTDOWN_TIMER,ONE_SECOND){
            override fun onTick(millisUntilFinished: Long) {
                currentTime.value = millisUntilFinished/ONE_SECOND
            }
            override fun onFinish() {
                gameFinish.value = true
                currentTime.value = 0L
            }
        }
        timer.start()
    }

    //FUNCIONES
    fun updateCountMovements (){
        countMovements.postValue(this.countMovements.value?.plus(1))
    }
    fun setPauseTime(){
        timer = object:CountDownTimer(currentTime.value!!*1000,ONE_SECOND){
            override fun onTick(millisUntilFinished: Long) {
                currentTime.value = millisUntilFinished/ONE_SECOND
            }

            override fun onFinish() {
                gameFinish.value = true
                currentTime.value = 0L
            }
        }
        timer.start()
    }
    override fun onCleared() {
        super.onCleared()
        timer.cancel()
    }

    fun girarCarta(idCarta:Int):Int{
        val card = cards[idCarta]
        if (indexOfSingleSelectedCard == null){
            // 0 o 2 cartas seleccionadas previamente
            restoreCards()
            indexOfSingleSelectedCard = idCarta
        }else{
            // solo 1 carta seleccionada previamente
            checkForMatch(indexOfSingleSelectedCard!!, idCarta)
            indexOfSingleSelectedCard = null
        }
        card.isFaceUp = !card.isFaceUp
        updateCountMovements()
        if (cards[idCarta].isFaceUp) {
            cards[idCarta].isFaceUp = true
            return cards[idCarta].resId
        } else {
            cards[idCarta].isFaceUp = false
            return R.drawable.card_image
        }
    }

   fun finalJoc():Boolean{
       if (countCardMatcheds==images.size){
           // 2 cartas destapadas son pareja -> +20pts
           // 2 cartasd destapadas no son pareja -> -5pts
           // Segundos restantes del crono se suman a los puntos
           //score = (countMatcheds*20)+(countNoMatcheds*(-5))
           timer.cancel()
           calculeScore()
           return true
       }
       else return false
   }

    fun updateView(idCarta: Int):Int{
        if (cards[idCarta].isFaceUp)return cards[idCarta].resId
        else return R.drawable.card_image
    }

    fun updateModels(idCarta: Int){
        val card = cards[idCarta]
        if (indexOfSingleSelectedCard == null){
            // 0 o 2 cartas seleccionadas previamente
            restoreCards()
            indexOfSingleSelectedCard = idCarta
        }else{
            // solo 1 carta seleccionada previamente
            checkForMatch(indexOfSingleSelectedCard!!, idCarta)
            indexOfSingleSelectedCard = null
        }
        card.isFaceUp = !card.isFaceUp
        updateCountMovements()
    }

    private fun checkForMatch(position1: Int, position2: Int) {
        if (cards[position1].id == cards[position2].id){
            cards[position1].isMatched = true
            cards[position2].isMatched = true
            countCardMatcheds += 2
            countMatcheds ++

        }else countNoMatcheds++
    }

    private fun calculeScore() {
        score = (countMatcheds*20)+(countNoMatcheds*(-5)+ currentTime.value!!.toInt())
    }

    private fun restoreCards() {
        for (card in cards){
            if (!card.isMatched){
                card.isFaceUp = false
            }
        }
    }
}
