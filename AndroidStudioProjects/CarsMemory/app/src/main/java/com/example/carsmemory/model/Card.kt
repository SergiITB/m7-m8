package com.example.carsmemory.model

data class Card (var id:Int, var resId: Int , var isFaceUp: Boolean = false, var isMatched: Boolean = false)