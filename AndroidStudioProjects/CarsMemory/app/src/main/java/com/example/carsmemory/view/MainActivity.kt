package com.example.carsmemory.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import com.example.carsmemory.R
import com.example.carsmemory.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {

        setTheme(R.style.SplashTheme)

        super.onCreate(savedInstanceState)

        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val levels = resources.getStringArray(R.array.Levels)
        val spinner = binding.spinner
        if (spinner != null) {
            val adapter = ArrayAdapter(
                this,
                android.R.layout.simple_spinner_item, levels
            )
            spinner.adapter = adapter
        }

        val playButton = binding.playButton
        playButton.setOnClickListener {
            if (spinner.selectedItem.equals("Easy")) {
                val intent = Intent(this, easyGame::class.java)
                startActivity(intent)
            } else if (spinner.selectedItem.equals("Hard")) {
                val intent = Intent(this, HardGame::class.java)
                startActivity(intent)
            }
        }

        val helpButton = binding.helpButton
        val helpText = binding.helpText
        val helpBackground = binding.helpBackground
        val helpCloseButton = binding.helpCloseButton
        helpButton.setOnClickListener {
            helpBackground.visibility = View.VISIBLE
            helpText.visibility=View.VISIBLE
            helpCloseButton.visibility=View.VISIBLE
            playButton.setOnClickListener {  }
            helpCloseButton.setOnClickListener {
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
       }
    }
}