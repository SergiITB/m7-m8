package com.example.carsmemory.view

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.carsmemory.R
import com.example.carsmemory.databinding.ActivityHardGameBinding
import com.example.carsmemory.databinding.ActivityResultScreenBinding

class resultScreen : AppCompatActivity() {
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = ActivityResultScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val bundle: Bundle? = intent.extras
        val score = bundle?.getString("score")
        val levelName = bundle?.getString("levelName")

        val finalScore = binding.scoreText
        finalScore.text = "SCORE\n $score Pts."

        val playAgainButton = binding.playAgainButton
        playAgainButton.setOnClickListener {
            if(levelName.equals("EasyGame")){
                val intent = Intent(this, easyGame::class.java)
                startActivity(intent)
            }else if (levelName.equals("HardGame")){
                val intent = Intent(this, HardGame::class.java)
                startActivity(intent)
            }
        }

        val menuButton = binding.menuButton
        menuButton.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        val shareButton = binding.shareButton
        shareButton.setOnClickListener {
            val intent = Intent(Intent.ACTION_SEND)
            intent.putExtra(Intent.EXTRA_TEXT, "My pts. in CarsMemory: $score !!\nTry to beat me ;-)")
            intent.type = "text/plain"
            startActivity(intent)
        }
    }
}