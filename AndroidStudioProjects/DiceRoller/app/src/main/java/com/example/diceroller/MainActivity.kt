package com.example.diceroller

import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.animation.AnimationUtils.loadAnimation

import android.widget.*
import com.example.diceroller.databinding.ActivityMainBinding
import com.google.android.material.animation.AnimationUtils

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val button : Button = binding.button
        val buttonReset : Button = binding.buttonReset
        val dice1 : ImageView = binding.imageView
        val dice2 : ImageView = binding.imageView2

        button.setOnClickListener{
            val randomNumber = (1..6).random()
            changeDiceImage(randomNumber, dice1)
            val randomNumber2 = (1..6).random()
            changeDiceImage(randomNumber2, dice2)
            if (randomNumber==6 && randomNumber2==6) {
                Toast.makeText(this, "JACKPOT", Toast.LENGTH_SHORT).show()
            }
        }

        buttonReset.setOnClickListener{
            changeDiceImage(0, dice1)
            changeDiceImage(0, dice2)
        }

        dice1.setOnClickListener{
            val randomNumber = (1..6).random()
            changeDiceImage(randomNumber, dice1)
        }

        dice2.setOnClickListener{
            val randomNumber = (1..6).random()
            changeDiceImage(randomNumber, dice2)
        }
    }
    fun changeDiceImage(randomNumber: Int, dice: ImageView){
        val images: Array<Int> = arrayOf(R.drawable.empty_dice, R.drawable.dice_1, R.drawable.dice_2, R.drawable.dice_3,
            R.drawable.dice_4, R.drawable.dice_5, R.drawable.dice_6)
        dice.setImageResource(images[randomNumber])
        dice.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake))
        val mp:MediaPlayer = MediaPlayer.create(this, R.raw.diceroll)
        mp.start()
    }
}