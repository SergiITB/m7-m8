package com.example.gestitb.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.gestitb.R
import com.example.gestitb.databinding.ItemLackAssistanceBinding
import com.example.gestitb.model.LackAssistance

class LackAssistanceAdapter (private val lackAssistances: List<LackAssistance> ):
    RecyclerView.Adapter<LackAssistanceAdapter.ViewHolder>(){
        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val binding = ItemLackAssistanceBinding.bind(view)
        }

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_lack_assistance, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val lackAssistance = lackAssistances[position]
        with(holder){
            binding.tvName.text = lackAssistance.name
            binding.tvModule.text = lackAssistance.details
            binding.tvDate.text = lackAssistance.date.toString()
            binding.ckeckBox.isChecked = lackAssistance.justified
        }
    }

    override fun getItemCount(): Int {
        return lackAssistances.size
    }
}