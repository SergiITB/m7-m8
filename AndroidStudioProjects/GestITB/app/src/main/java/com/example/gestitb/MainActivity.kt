package com.example.gestitb

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.gestitb.fragments.GestFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, GestFragment())
            commit()
        }
    }
}