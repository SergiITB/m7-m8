package com.example.gestitb.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.gestitb.R
import com.example.gestitb.adapter.LackAssistanceAdapter
import com.example.gestitb.databinding.FragmentGestBinding
import com.example.gestitb.databinding.ItemLackAssistanceBinding
import com.example.gestitb.model.LackAssistance
import java.util.*


class GestFragment : Fragment() {

    private lateinit var lackAssistanceAdapter: LackAssistanceAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    private lateinit var binding: FragmentGestBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? { // Inflate the layout for this fragment
        binding= FragmentGestBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lackAssistanceAdapter = LackAssistanceAdapter(getLackAssistances())
        linearLayoutManager = LinearLayoutManager(context)

        binding.recyclerView.apply {
            setHasFixedSize(true) //Optimitza el rendiment de l’app
            layoutManager = linearLayoutManager
            adapter = lackAssistanceAdapter
        }
    }

    private fun getLackAssistances(): MutableList<LackAssistance>{
        val lackAssistances = mutableListOf<LackAssistance>()
        lackAssistances.add(LackAssistance(1, "Sergi","M8", Date(),true))
        lackAssistances.add(LackAssistance(2, "David","M7", Date(),false))
        return lackAssistances
    }
}