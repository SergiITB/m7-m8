package com.example.gestitb.model

import java.util.*

data class LackAssistance(
    val id: Long, var name: String, var details: String,
    var date: Date, var justified:Boolean)
