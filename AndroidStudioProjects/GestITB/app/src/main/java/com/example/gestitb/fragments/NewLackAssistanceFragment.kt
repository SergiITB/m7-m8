package com.example.gestitb.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.gestitb.R
import com.example.gestitb.adapter.LackAssistanceAdapter
import com.example.gestitb.databinding.FragmentGestBinding
import com.example.gestitb.databinding.FragmentNewLackAssistanceBinding
import com.example.gestitb.model.LackAssistance
import java.util.*

//private lateinit var lackAssistanceAdapter: LackAssistanceAdapter
//private lateinit var linearLayoutManager: RecyclerView.LayoutManager
//private lateinit var binding: FragmentNewLackAssistanceBinding
//private lateinit var binding2: FragmentGestBinding
//val spinner = binding.spinner

class NewLackAssistanceFragment : Fragment() {

//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        val details = resources.getStringArray(R.array.Details)
//        if (spinner != null) {
//            val adapter = ArrayAdapter(
//                this,
//                android.R.layout.simple_spinner_item, details)
//            spinner.adapter = adapter
//        }
//    }
//
//    override fun onCreateView(
//        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? { // Inflate the layout for this fragment
//        binding= FragmentNewLackAssistanceBinding.inflate(layoutInflater)
//        return inflater.inflate(R.layout.fragment_gest_, container, false)
//    }
//
//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//        lackAssistanceAdapter = LackAssistanceAdapter(getLackAssistances())
//        linearLayoutManager = LinearLayoutManager(context)
//
//        binding2.recyclerView.apply {
//            setHasFixedSize(true) //Optimitza el rendiment de l’app
//            layoutManager = linearLayoutManager
//            adapter = lackAssistanceAdapter
//        }
//    }
//
//
//
//    private fun getLackAssistances(): MutableList<LackAssistance>{
//        val name = binding.editTextName
//        val justified = binding.justifiedCheck
//        val lackAssistances = mutableListOf<LackAssistance>()
//        lackAssistances.add(LackAssistance(1, name.text.toString(),spinner.selectedItem.toString(), Date(),justified.isChecked))
//        return lackAssistances
//    }
}