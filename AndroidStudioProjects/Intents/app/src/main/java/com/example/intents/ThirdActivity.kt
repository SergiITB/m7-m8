package com.example.intents

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.intents.databinding.ActivityMainBinding
import com.example.intents.databinding.ActivitySecondBinding
import com.example.intents.databinding.ActivityThirdBinding

class ThirdActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val bundle: Bundle? = intent.extras
        val name = bundle?.getString("name")

        val binding = ActivityThirdBinding.inflate(layoutInflater)
        val secondBinding = ActivitySecondBinding.inflate(layoutInflater)
        val buttonShare = binding.buttonShare
        val buttonShow = binding.buttonShow
        setContentView(binding.root)

        buttonShow.setOnClickListener{
            val option1 = secondBinding.radioBye
            val option2 = secondBinding.radioHello
            val age = secondBinding.seekBarAgeText.text
            binding.nameResultText.text = "Dades introduides:\nNom: $name"
            binding.ageResultText.text = "Edat: $age anys"
            if (option1.isChecked){
                binding.tipeResultText.text = option1.text.toString()
            }else binding.tipeResultText.text = option2.text.toString()

        }

        buttonShare.setOnClickListener {
            val intent = Intent(Intent.ACTION_SEND)
            intent.putExtra(Intent.EXTRA_TEXT, "Missatge")
            intent.type = "text/plain"
            startActivity(intent)
        }
    }
}