package com.example.intents

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.SeekBar
import com.example.intents.databinding.ActivitySecondBinding
import com.example.intents.databinding.ActivityThirdBinding

class SecondActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = ActivitySecondBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val seekBar = binding.seekBarAge
        val next = binding.buttonGoTo3

        val bundle: Bundle? = intent.extras
        val name = bundle?.getString("name")

        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean){
                binding.seekBarAgeText.text = p1.toString()
            }
            override fun onStartTrackingTouch(p0: SeekBar?){}
            override fun onStopTrackingTouch(p0: SeekBar?){}
        })

        next.setOnClickListener {
            val intent = Intent(this, ThirdActivity::class.java)
            intent.putExtra("name", name)
            startActivity(intent)
        }
    }
}