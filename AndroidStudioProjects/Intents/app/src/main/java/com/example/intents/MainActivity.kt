package com.example.intents

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.intents.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val next = binding.buttonGoTo2
        val name = binding.editTextName


        next.setOnClickListener {
            val intent = Intent(this, SecondActivity::class.java)
            intent.putExtra("name", name.text.toString())
            startActivity(intent)
        }
    }
}