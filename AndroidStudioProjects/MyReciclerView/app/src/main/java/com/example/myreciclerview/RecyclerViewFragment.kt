package com.example.myreciclerview

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myreciclerview.databinding.FragmentRecyclerViewBinding


class RecyclerViewFragment : Fragment() {

    private lateinit var userAdapter: UserAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    private lateinit var binding: FragmentRecyclerViewBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding= FragmentRecyclerViewBinding.inflate(layoutInflater)
        return  binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        userAdapter = UserAdapter(getUsers())
        linearLayoutManager = LinearLayoutManager(context)

        binding.recyclerView.apply {
            setHasFixedSize(true) //Optimitza el rendiment de l’app
            layoutManager = linearLayoutManager
            adapter = userAdapter
        }
    }

    private fun getUsers(): MutableList<User>{
        val users = mutableListOf<User>()
        users.add(User(1, "Batman", "https://i.blogs.es/24bcec/the-batman-affleck/840_560.jpg"))
        users.add(User(2, "Deadpool", "https://depor.com/resizer/hjJYlZwph43h7dL89h8oMOO5f3o=/580x330/smart/filters:format(jpeg):quality(75)/cloudfront-us-east-1.images.arcpublishing.com/elcomercio/YGPFSKUMXVBNBPWISJH3XPAGV4.jpg"))
        users.add(User(3, "Leonardo", "https://800noticias.com/cms/wp-content/uploads/2015/03/leonardoturtle.jpg"))
        users.add(User(1, "Batman", "https://i.blogs.es/24bcec/the-batman-affleck/840_560.jpg"))
        users.add(User(2, "Deadpool", "https://depor.com/resizer/hjJYlZwph43h7dL89h8oMOO5f3o=/580x330/smart/filters:format(jpeg):quality(75)/cloudfront-us-east-1.images.arcpublishing.com/elcomercio/YGPFSKUMXVBNBPWISJH3XPAGV4.jpg"))
        users.add(User(3, "Leonardo", "https://800noticias.com/cms/wp-content/uploads/2015/03/leonardoturtle.jpg"))
        users.add(User(1, "Batman", "https://i.blogs.es/24bcec/the-batman-affleck/840_560.jpg"))
        users.add(User(2, "Deadpool", "https://depor.com/resizer/hjJYlZwph43h7dL89h8oMOO5f3o=/580x330/smart/filters:format(jpeg):quality(75)/cloudfront-us-east-1.images.arcpublishing.com/elcomercio/YGPFSKUMXVBNBPWISJH3XPAGV4.jpg"))
        users.add(User(3, "Leonardo", "https://800noticias.com/cms/wp-content/uploads/2015/03/leonardoturtle.jpg"))
        users.add(User(1, "Batman", "https://i.blogs.es/24bcec/the-batman-affleck/840_560.jpg"))
        users.add(User(2, "Deadpool", "https://depor.com/resizer/hjJYlZwph43h7dL89h8oMOO5f3o=/580x330/smart/filters:format(jpeg):quality(75)/cloudfront-us-east-1.images.arcpublishing.com/elcomercio/YGPFSKUMXVBNBPWISJH3XPAGV4.jpg"))
        users.add(User(3, "Leonardo", "https://800noticias.com/cms/wp-content/uploads/2015/03/leonardoturtle.jpg"))
        users.add(User(1, "Batman", "https://i.blogs.es/24bcec/the-batman-affleck/840_560.jpg"))
        users.add(User(2, "Deadpool", "https://depor.com/resizer/hjJYlZwph43h7dL89h8oMOO5f3o=/580x330/smart/filters:format(jpeg):quality(75)/cloudfront-us-east-1.images.arcpublishing.com/elcomercio/YGPFSKUMXVBNBPWISJH3XPAGV4.jpg"))
        users.add(User(3, "Leonardo", "https://800noticias.com/cms/wp-content/uploads/2015/03/leonardoturtle.jpg"))
        users.add(User(1, "Batman", "https://i.blogs.es/24bcec/the-batman-affleck/840_560.jpg"))
        users.add(User(2, "Deadpool", "https://depor.com/resizer/hjJYlZwph43h7dL89h8oMOO5f3o=/580x330/smart/filters:format(jpeg):quality(75)/cloudfront-us-east-1.images.arcpublishing.com/elcomercio/YGPFSKUMXVBNBPWISJH3XPAGV4.jpg"))
        users.add(User(3, "Leonardo", "https://800noticias.com/cms/wp-content/uploads/2015/03/leonardoturtle.jpg"))
        users.add(User(1, "Batman", "https://i.blogs.es/24bcec/the-batman-affleck/840_560.jpg"))
        users.add(User(2, "Deadpool", "https://depor.com/resizer/hjJYlZwph43h7dL89h8oMOO5f3o=/580x330/smart/filters:format(jpeg):quality(75)/cloudfront-us-east-1.images.arcpublishing.com/elcomercio/YGPFSKUMXVBNBPWISJH3XPAGV4.jpg"))
        users.add(User(3, "Leonardo", "https://800noticias.com/cms/wp-content/uploads/2015/03/leonardoturtle.jpg"))
        return users
    }

}