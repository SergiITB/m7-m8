package com.example.myreciclerview

interface OnClickListener {
    fun onClick(user: User)
}
