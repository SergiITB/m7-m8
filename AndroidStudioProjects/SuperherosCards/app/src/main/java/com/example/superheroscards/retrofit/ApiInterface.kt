package com.example.superheroscards.retrofit

import com.example.superheroscards.models.Superhero
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Url

interface ApiInterface {
    @GET()
    suspend fun getData(@Url url: String): Response<Superhero>

    companion object {
        val BASE_URL = "https://akabab.github.io/superhero-api/api/"

        fun create(): ApiInterface {
            val client = OkHttpClient.Builder().build()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
            return retrofit.create(ApiInterface::class.java)
        }
    }
}