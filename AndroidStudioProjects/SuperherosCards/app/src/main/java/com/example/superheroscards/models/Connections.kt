package com.example.superheroscards.models

data class Connections(
    val groupAffiliation: String,
    val relatives: String
)