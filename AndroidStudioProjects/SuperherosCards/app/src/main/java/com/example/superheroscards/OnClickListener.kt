package com.example.superheroscards

interface OnClickListener {
    fun onClick(superhero: SuperheroGeneral)
}