package com.example.superheroscards

import com.example.superheroscards.models.SuperheroItem
import com.example.superheroscards.room.SuperheroEntity

data class SuperheroGeneral(
    var id: Long,
    var name:String,
    var image: String,
    var intelligence: Int,
    var strenght: Int,
    var speed:Int,
    var durability:Int,
    var power:Int,
    var combat: Int,
    var gender:String,
    var race:String,
    var height:String,
    var weight:String,
    var alterEgos: String,
    var aliases:String,
    var placeOfBirth:String,
    var firstAppearance:String,
    var publisher:String,
    var occupation:String,
    var fullName:String)

fun SuperheroItem.toSuperheroGeneral() = SuperheroGeneral(id.toLong(),name,images.md,powerstats.intelligence,powerstats.strength,powerstats.speed,
    powerstats.durability,powerstats.power,powerstats.combat,appearance.gender,
    (appearance.race ?: "").toString(),
    (appearance.height ?: "").toString(),
    (appearance.weight ?: "").toString(),
    biography.alterEgos,biography.aliases.toString(),biography.placeOfBirth,biography.firstAppearance,biography.publisher ?: "",work.occupation,
    biography.fullName)
fun SuperheroEntity.toSuperheroGeneral() = SuperheroGeneral(id,name,image,intelligence,strenght,speed,
    durability,power,combat,gender,race,height,weight,
    alterEgos,aliases,placeOfBirth,firstAppearance,publisher,occupation,
    fullName)

