package com.example.superheroscards.room

import android.app.Application
import androidx.room.Room

class SuperheroApplication : Application(){
    companion object {
        lateinit var database: SuperheroDatabase
    }

    override fun onCreate() {
        super.onCreate()
        database = Room.databaseBuilder(this,
            SuperheroDatabase::class.java,
            "SuperheroDatabase").build()
    }

}