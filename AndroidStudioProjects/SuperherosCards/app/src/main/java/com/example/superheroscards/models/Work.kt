package com.example.superheroscards.models

data class Work(
    val base: String,
    val occupation: String
)