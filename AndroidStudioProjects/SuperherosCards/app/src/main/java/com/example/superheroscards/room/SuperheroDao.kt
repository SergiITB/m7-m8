package com.example.superheroscards.room

import androidx.room.*

@Dao
interface SuperheroDao {
    @Query("SELECT * FROM SuperheroEntity")
    fun getAllSuperheros(): MutableList<SuperheroEntity>
    @Insert
    fun addSuperhero(superheroEntity: SuperheroEntity)
    @Update
    fun updateSuperhero(superheroEntity: SuperheroEntity)
    @Delete
    fun deleteSuperhero(superheroEntity: SuperheroEntity)
    @Query("SELECT COUNT(*) FROM SuperheroEntity WHERE id = :id")
    fun isFavorite(id: Int): Int
}