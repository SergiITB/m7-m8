package com.example.superheroscards.models

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.superheroscards.Repository
import com.example.superheroscards.SuperheroGeneral
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SuperheroViewModel: ViewModel() {
    var repository = Repository()
    var data = MutableLiveData<List<SuperheroGeneral>>()
    var dataRoom = MutableLiveData<List<SuperheroGeneral>>()
    var actualSuperhero = MutableLiveData<SuperheroGeneral>()

    init {
        fetchData("all.json")
    }

    fun fetchData(url: String){
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO){ repository.fetchDataFromApi(url) }
            data.postValue(response)
        }
    }

    fun fetchDataRoom(){
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO){repository.fetchDataFromRoom()}
            dataRoom.postValue(response)
        }
    }

    fun setSuperhero(superhero: SuperheroGeneral){
        actualSuperhero.value = superhero
    }
}