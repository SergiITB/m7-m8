package com.example.superheroscards.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.superheroscards.OnClickListener
import com.example.superheroscards.R
import com.example.superheroscards.SuperheroGeneral
import com.example.superheroscards.databinding.ItemSuperheroBinding

class SuperheroAdapter (private val superheros:List<SuperheroGeneral>, private val listener: OnClickListener):
    RecyclerView.Adapter<SuperheroAdapter.ViewHolder>(){

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_superhero, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val superhero = superheros[position]
        val image: String = superhero.image
        with(holder){
            setListener(superhero)
            binding.textViewName.text = superhero.name
            Glide.with(context)
                .load(image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .circleCrop()
                .into(binding.imgPhoto)
        }
    }

    override fun getItemCount(): Int {
        return superheros.size
    }

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ItemSuperheroBinding.bind(view)

        fun setListener(superhero: SuperheroGeneral){
            binding.root.setOnClickListener {
                listener.onClick(superhero)
            }
        }
    }
}


