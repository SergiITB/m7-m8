package com.example.superheroscards

import com.example.superheroscards.models.SuperheroItem
import com.example.superheroscards.retrofit.ApiInterface
import com.example.superheroscards.room.SuperheroApplication
import com.example.superheroscards.room.SuperheroEntity

class Repository {
    private val apiInterface = ApiInterface.create()

    suspend fun fetchDataFromApi(url: String): List<SuperheroGeneral> {
        val response = apiInterface.getData(url)
        lateinit var results: List<SuperheroItem>
        if(response.isSuccessful){
            results = response.body()!!
        }
        else{
            results = emptyList()
        }
        return results.map { it.toSuperheroGeneral() }
    }

    fun fetchDataFromRoom(): List<SuperheroGeneral> {
        val response: List<SuperheroEntity> = SuperheroApplication.database.superheroDao().getAllSuperheros()
        return response.map { it.toSuperheroGeneral() }
    }
}