package com.example.superheroscards.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.superheroscards.R
import com.example.superheroscards.databinding.FragmentSuperHeroDetailsBinding
import com.example.superheroscards.models.*
import com.example.superheroscards.room.SuperheroApplication
import com.example.superheroscards.room.SuperheroEntity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SuperHeroDetailsFragment : Fragment() {

    private val model: SuperheroViewModel by activityViewModels()
    lateinit var binding: FragmentSuperHeroDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentSuperHeroDetailsBinding.inflate(layoutInflater)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        
        model.actualSuperhero.observe(viewLifecycleOwner, Observer {
            val superhero = it
            val image: String = superhero.image

            CoroutineScope(Dispatchers.IO).launch {
                binding.favoriteIcon.isChecked = SuperheroApplication.database.superheroDao().isFavorite(it.id.toInt()) != 0
            }

            Glide.with(requireContext())
                .load(image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(binding.superHeroImg)
            binding.nameText.text = superhero.name
            binding.intelligenceProgressBar.progress = superhero.intelligence
            binding.intelligenceNumber.text = superhero.intelligence.toString()
            binding.strenghtProgressBar.progress = superhero.strenght
            binding.strenghtNumber.text = superhero.strenght.toString()
            binding.speedProgressBar.progress = superhero.speed
            binding.speedNumber.text = superhero.speed.toString()
            binding.durabilityProgressBar.progress = superhero.durability
            binding.durabilityNumber.text = superhero.durability.toString()
            binding.powerProgressBar.progress = superhero.power
            binding.powerNumber.text = superhero.power.toString()
            binding.combatProgressBar.progress = superhero.combat
            binding.combatNumber.text = superhero.combat.toString()

            binding.gender.text = "GENDER: "+superhero.gender
            binding.race.text = "RACE: "+superhero.race
            binding.height.text = "HEIGHT: "+superhero.height
            binding.weight.text = "WEIGHT"+superhero.weight
            binding.alterEgos.text = "ALTER-EGOS: "+superhero.alterEgos
            binding.aliases.text = "ALIASES: "+superhero.aliases
            binding.placeOfBirth.text = "PLACE OF BIRTH: "+superhero.placeOfBirth
            binding.firstAppearance.text = "FIRST APPEARANCE: "+superhero.firstAppearance
            binding.publisher.text = "PUBLISHER: "+superhero.publisher
            binding.occupationText.text = "OCUPATION: "+superhero.occupation
            binding.fullNameText.text = "REAL NAME: "+superhero.fullName
        })

        binding.homeIcon.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, RecyclerViewFragment())
                setReorderingAllowed(true)
                addToBackStack(null)
                commit()
            }
        }

        binding.favoriteIcon.setOnClickListener{
            val superhero = model.actualSuperhero.value
            val id = superhero!!.id
            val name = superhero.name
            val image = superhero.image
            val intelligence = superhero.intelligence
            val strenght = superhero.strenght
            val speed = superhero.speed
            val durability = superhero.durability
            val power = superhero.power
            val combat = superhero.combat
            val gender = superhero.gender
            val race = superhero.race
            val height = superhero.height
            val weight = superhero.weight
            val alterEgos = superhero.alterEgos
            val aliases = superhero.aliases
            val placeOfBirth = superhero.placeOfBirth
            val firstAppearance = superhero.firstAppearance
            val publisher = superhero.publisher
            val occupation = superhero.occupation
            val fullName = superhero.fullName
            val superheroEntity = SuperheroEntity(id, name,image,intelligence,strenght,speed,
                durability,power,combat,gender,race,height,weight, alterEgos,aliases,placeOfBirth,
                firstAppearance,publisher,occupation,fullName)

            CoroutineScope(Dispatchers.IO).launch {
                if (SuperheroApplication.database.superheroDao().isFavorite(id.toInt()) == 0){
                    SuperheroApplication.database.superheroDao().addSuperhero(superheroEntity)
                }else{
                    SuperheroApplication.database.superheroDao().deleteSuperhero(superheroEntity)
                }
            }
        }
    }
}