package com.example.superheroscards.fragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.superheroscards.OnClickListener
import com.example.superheroscards.R
import com.example.superheroscards.SuperheroGeneral
import com.example.superheroscards.adapters.SuperheroAdapter
import com.example.superheroscards.databinding.FragmentFavoritesBinding
import com.example.superheroscards.models.SuperheroViewModel


class FavoritesFragment : Fragment(), OnClickListener{

    private lateinit var superheroAdapter: SuperheroAdapter
    private lateinit var binding: FragmentFavoritesBinding
    private val model: SuperheroViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentFavoritesBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        model.fetchDataRoom()
        model.dataRoom.value?.let {
            setUpRecyclerView(it)
        }
        model.dataRoom.observe(viewLifecycleOwner, Observer {
            if(model.dataRoom.value == null){
                Toast.makeText(context, "ERROR", Toast.LENGTH_SHORT).show()
            }
            else{
                Log.e("CAMBIOS EN EL VIEWMODEL", model.dataRoom.value.toString())
                if (it != null) {
                    setUpRecyclerView(it)
                }
            }
        })
    }

    private fun setUpRecyclerView(myData: List<SuperheroGeneral>) {
        superheroAdapter = SuperheroAdapter(myData, this)
        binding.recycler.apply {
            setHasFixedSize(true) //Optimitza el rendiment de l’app
            val display: Display = (context.getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay
            val rotation: Int = display.getRotation()
            if(rotation ==0){
                layoutManager = LinearLayoutManager(context)
            }else{
                layoutManager = GridLayoutManager(context,2)
            }
            adapter = superheroAdapter
        }
    }

    override fun onClick(superhero: SuperheroGeneral) {
        model.setSuperhero(superhero)
        view?.let {
            Navigation.findNavController(it).navigate(R.id.action_favouritesFragment_to_superHeroDetailsFragment)
        }
    }
}