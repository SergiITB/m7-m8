package com.example.superheroscards.room

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "SuperheroEntity")
data class SuperheroEntity(
    @PrimaryKey(autoGenerate = true)
    var id: Long,
    var name:String,
    var image: String,
    var intelligence: Int,
    var strenght: Int,
    var speed:Int,
    var durability:Int,
    var power:Int,
    var combat: Int,
    var gender: String,
    var race:String,
    var height:String,
    var weight:String,
    var alterEgos: String,
    var aliases:String,
    var placeOfBirth:String,
    var firstAppearance:String,
    var publisher:String,
    var occupation:String,
    var fullName:String
)