package com.example.superheroscards.models

data class Images(
    val lg: String,
    val md: String,
    val sm: String,
    val xs: String
)