package com.example.superheroscards.room

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = arrayOf(SuperheroEntity::class), version = 1)
abstract class SuperheroDatabase : RoomDatabase(){
    abstract fun superheroDao():SuperheroDao
}