package com.example.matchinggame

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class LaunchScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launch_screen)
    }
}